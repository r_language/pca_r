***PCA with R***

**Question 1**

Do the PCA on the excel file.

**Question 2**

Which information can we deduce with the file's summary ?

**Question 3**

Note the correlation coefficient the most important and give an interpretation of the correlation coefficients.

**Question 4**

How many factorial axis or main components can we retain and why ?

**Question 5**

Give the projected inertia in each factorial axis retained.

**Question 6**

Interpret the first 3 axis considering the topics, not the individuals.

**Question 7**

Trace and analyze the scatter plot of the 27 individuals in the factorial plans 1-2 and 1-3.

**Question 8 and 9**

Which phenomena can we deduce with the correlation circle with the first 2 factors ? Which topics are represented in the best way in this plan and why ? 

**Question 10**

Calculate and interpret the constributions for the individuals number 10 and 12 on the factorial axis 1 and 2.

**Question 11**

Which of the individuals explain the factorial axis 1,2 and 3 ? 


**Question 12**

Can we sort individuals into groups ?


Author Marion Estoup

E-mail : marion_110@hotmail.fr

December 2020

