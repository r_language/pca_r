---
title: "PCA and Clustering Master Health Data Science"
author: "Marion Estoup"
date: "December 2020"
E-mail: "marion_110@hotmail.fr"
output: pdf_document
---

# **Objectives**
We have a work to realize which is doing a PCA and a clustering with R language thanks to an excel file that contains pupils and topics. The file has pupil's grades in the different topics. To guide us, we have 12 questions which we have to answer.

## **Data Management**

### Code that shows the load of the libraries
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library(readxl)
library(Hmisc) 
library(ggplot2)
library(factoextra)
library(FactoMineR)
library(psych) 
```

### Code that shows the path and load of the file 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
path_import='/Users/marionestoup/Desktop/exercices info/Assi_NGuessan'
file_eleves = read_excel(file.path(path_import, 'Data_eleves.xls'))

```
In the file, I noticed that the last column/variable (number 16) was full of NA including the last 3 observations/rows. So I chose to remove them.

### Code that shows the removal of variables and observations with NA
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
file_eleves <- file_eleves[,-16]
file_eleves <- file_eleves[-28,]
file_eleves <- file_eleves[-28,]
file_eleves <- file_eleves[-28,]
```

### Code that shows the first rows and the structure of the file
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
head(file_eleves)
str(file_eleves)

```
We can notice that our file contains almost only numeric variables except for the first variable «élève» that is a character chain. So I’m going to convert it into numeric to be able to realize PCA afterwards.

### Code that shows the conversion of the variable élèves into a caracter chain
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
file_eleves$eleves=c("1","2","3","4","5","6","7","8","9","10","11","12","13",
                     "14","15","16","17","18","19","20","21","22","23","24","25",
                     "26","27")

class(file_eleves$eleves) # caractère
file_eleves$eleves <- as.numeric(file_eleves$eleves)
class(file_eleves$eleves) # numérique

```

## **Question 1**
Do the PCA on the file.
To do so, I have to select active variables in order to have individuals on one side and the variables that qualify the individuals on the other side (the topics).

### Code that shows the selection of active variables and PCA on the file 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
variable_active <- file_eleves[1:27,2:15]
pca <- PCA(variable_active, graph=TRUE)
```
We obtain the PCA, a scatter plot with the individuals on the first and second dimensions. In addition, we have a correlation circle graph on the first and second dimensions. On these graphs we have two components which are the dimension 1 and dimension 2 that represent about 51% of the file (34.11 + 17.09).

## **Question 2**
Which information can we deduce with the file’s summary ?
First of all, I’m going to explain this summary. 
After, I’m going to explain more precisely the PCA’s summary on the file.

### Code that shows the file’s summary
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
summary(file_eleves)
```
Like I converted the variable élèves, we see that it’s numeric and all our data is quantitative. Indeed, we have numbers with min, max, mean, median, etc.

### Code that shows the PCA’s summary on the file
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
summary(pca,nbelements=27)

```
With this summary we can see the variances, the variance’s percentages, the cumulated total of those percentages in 14 dimensions. 
For example, we can say that about 34% of the total variance is explained by the first dimension, that about 51% of the total variance is explained by both dimensions 1 and 2, etc. 

To continue, we obtain data on the individuals and on the variables. 
There is the distance from the gravity center concerning the individuals, but also their coordinates, their contributions in each dimension, and their cos2 in the first three dimensions.

Concerning the variables, we also have their coordinates, their contribution’s percentages, their cos2 in the first three dimensions. 
The cos2 is the quality of representation. In this way, more its value is high, more the individuals are well projected on the plan and in the space. The other way around, more the cos2 value is low, more individuals are badly projected on the plan. 

Interpretation : 
We can calculate the mean contribution of individuals : 100/27= around 4. So we can select individuals that have a contribution higher than the average contribution in each dimension. It’s the case for the individuals 1, 9, 10, 12, 21 and 24 in the first dimension. It’s the individuals 1, 10, 13, 16, 17, 19, 21, 22, 23 and 26 in the dimension 2, etc. 
Then we can check the cos2 values of these individuals that have a contribution higher than the average contribution. In the first dimension for example, the individuals 1, 9, 10, 12, 21, et 24 have a contribution higher than the average contribution and they also have the highest cos2 values. So the first dimension is mainly represented by those individuals. After we can do the same for the second and third dimension. 

In the same way for the variables, the average contribution is 100/14 = around 7. So we can select the topics that have a contribution higher than the average contribution in each dimension. It’s the case for the topics orth, expr, math, angl, hist, biol, edmu, arts, expo in the first dimension. Then we have to look for the second and third. 
After, we can check the cos2 values corresponding to the topics that have a contribution higher than the average contribution. For example, the topics orth, expr, math, angl, hist, edmu, et arts in the first dimension have their contribution higher than the average contribution and have the highest values of cos2. So the first dimension is mainly represented by those topics. After we can do the same for the second and third dimension.

Whether it’s for individuals or topics, we can determinate which variable or observation contribute the most to the dimensions 1-2 or 1-3. We can follow the same interpretation as before but we have to look in both dimensions at the same time.

## **Question 3**
Note the correlation coefficient the most important (test the coefficients significatives with an alpha risk of 5%). Then give an interpretation of the correlation coefficients. 
We can identify variables that are significantly associated with a principal component given thanks to their correlation coefficient with an alpha risk of 5%.

### Code that shows the correlation tab between variables 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
coeff_correlation <- rcorr(as.matrix(variable_active))$r
print(coeff_correlation)
```

### Code that shows the correlations and p values for dimensions 1, 2 and 3 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
dimdesc(pca)
```
More the correlation is close to 1, more the variable is correlated and more it explains the component that we observe. 
For example, we see that the variables correlated to the first dimension with an alpha risk of 5% are expr, arts, math, hist, angl, orth, edmu, biol, expo, geo et gram. They are positively correlated. Those which are the most correlated are expr, arts, math, hist, angl et orth that have a correlation higher or equal to 0.7.
Then we can do the same thing for the other dimensions.

## **Question 4**
How many factorial axis or main components can we retain and why ? 
The eigenvalues measure the quantity of variance explained by each main axis. We can examine them to determine the number of main components that we have to take into consideration. In addition, with the Kaiser criteria, we have to keep the axis which have an inertia higher to the average inertia, and we have to keep the axis associated with eigenvalues higher than 1. 

### Code that shows the PCA’s eigenvalues
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
valeur_propre <- get_eigenvalue(pca)
valeur_propre
```
Therefore, we have the eigenvalues, the variance percentages, and the cumulated percentages of the variance. 
The percentages correspond to the inertia percentages explained in each axis or dimension. We can say for example that around 51% of the total variance is explained by the first two dimensions. 
Like the eigenvalues of the first 5 main components are higher than 1, we can keep the first 5 dimensions/factorial axis.
We can also visualize the eigenvalues.

### Code that shows the bart chart with PCA’s eigenvalues 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_eig(pca, choice="eigenvalue", addlabels=TRUE, title="Valeurs propres de l ACP")+
  geom_hline(yintercept=1, linetype=2, color="red")
```
We see indeed that the eigenvalues higher than 1 concern the first 5 dimensions.

## **Question 5**
Give the projected inertia in each factorial axis retained.
Knowing that I kept the first 5 dimensions, we can focus on their inertia.

### Code that shows the bar chart for the first 10 factorial axis 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_eig(pca, addlabels=TRUE, ylim=c(0,50), title=
           "Projection de l'inertie sur les différents axes factoriels")
```
Thanks to this bar chart, we can say that the first factorial axis has an inertia of 34.1%, the second an inertia of 17.1%, the third an inertia of 10.7%, the fourth an inertia of 9.6% and the fifth an inertia of 8%.

## **Question 6**
Interpret the first 3 axis considering the topics, not the individuals. 
I’m going to extract the coordinates, cos2 and contribution percentages of each topics in the first 5 dimensions.

### Code that shows the coordinates, cos2 and contribution percentages 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
extraction_resultat <- get_pca_var(pca)
extraction_resultat
head(extraction_resultat,15)

```
Here, we can interpret in the same way than in the second question. We look and keep in each axis the topics that have a contribution higher than the average contribution. Then we observe their cos2 for the topics that have a contribution higher than the average contribution, and we keep the topics that have the highest cos2. 
The average contribution is 100/14= around 7. In the first dimension the topics orth, expr, math, angl, hist, biol, edmu, arts, expo have a contribution higher than the average contribution. Then we look their cos2. And we see that the topics orth, expr, math, angl, hist, edmu, et arts have a contribution higher than the average contribution and the highest cos2 values. 
We can do the same interpretation for the other axis/dimensions.

## **Question 7**
Trace and analyze the scatter plot of the 27 individuals in the factorial plans 1-2 and 1-3.

### Code that shows the access to the values of the individuals 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
resultat_individu <- get_pca_ind(pca)
resultat_individu 
#Pour accéder aux differents éléments on utilise 
#resultat_individu$coord        #Coordonnées des élèves
#resultat_individu$cos2         #Qualité des élèves
#resultat_individu$contrib     #Contributions des élèves
```
In the same way as before with the variables, we have here the coordinates, cos2, contribution percentages for the individuals in the first 5 axis. 
We can also visualize individuals with a scatter plot or a bar chart.

### Code that shows the scatter plots 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_pca_ind(pca)

fviz_pca_ind(pca, col.ind="contrib",
             gradient.cols= c("#00AFBB","#E7B800","#FC4E07"),
             repel=TRUE)

fviz_pca_ind(pca, col.ind="cos2",
             gradient.cols= c("#00AFBB","#E7B800","#FC4E07"),
             repel=TRUE)
```
Thanks to the graphics, we see that in the dimensions 1 and 2, the individuals that have the highest contribution are the individuals in orange/red (individuals number 12, 1, 23, 10, and 21).
Then we see that those who have the highest cos2 are also represented in orange/red.

### Code that shows a bar chart of the individuals with their contribution in the dimensions 1-2 and 1-3, and then a bar chart with their cos2 in the dimensions 1-2 and 1-3
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_contrib(pca, choice="ind", axes=1:2)
fviz_contrib(pca, choice="ind", axes=1:3)

fviz_cos2(pca, choice="ind", axes=1:2)
fviz_cos2(pca, choice="ind", axes=1:3)
```
The individuals that have a contribution higher than the average contribution in the axis 1-2 are the individuals 10, 12, 21, 1, 23, 24, 13 and 9. Among them, those who have a high value of cos 2 are the individuals 21, 10, 9, 1, 12, 22, 13, 19, 5, 23 and 16. So those who have a high contribution and high cos2 are the individuals 10, 12, 21, 1, 23, 13 and 9 in the axis 1-2. So this dimension is mainly explained by those individuals. 
We can do the same functioning for the dimension/axis 1-3.

## **Question 8 and 9**
Which phenomena can we deduce with the correlation circle with the first 2 factors ? 
Which topics are represented in the best way in this plan and why ? 
Having a high value of cos2 means that the variables have a good representation on the main axis. The other way around, a low value of cos2 means that the variables aren’t perfectly represented on the main axis. 

### Code that shows the correlation circle of the first 2 factorial axis with colors 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_pca_var(pca, col.var="contrib", 
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE 
             )

fviz_pca_var(pca, col.var="cos2", 
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE 
             )
```
The topics/variables are all on the same side. Therefore, they are positively correlated. This phenomena usually happens on the first factorial axis, we can talk of size factor. In this way, we can talk about the shape factor on the second factorial axis. Variables far from the origin and close to the factorial axis are well represented by the PCA. So we can say that the topics edmu, arts, expr, orth et angl seem to be well represented on the first 2 dimensions. 
In addition of the correlation circle, we can visualize contribution values and cos2 values the topics thanks to a tab or a bar chart. 

### Code that shows the contribution and cos2 values in a tab for the first 5 dimensions 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library("corrplot")
corrplot(extraction_resultat$contrib, is.corr=FALSE)
corrplot(extraction_resultat$cos2, is.corr=FALSE)
```

### Code that shows the bar charts for the contribution and cos2 values on the factorial axis 1-2, 1-2-3, 1-2-3-4 and 1-2-3-4-5 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_contrib(pca, choice="var", axes=1:2)
fviz_cos2(pca, choice="var", axes=1:2)

fviz_contrib(pca, choice="var", axes=1:3)
fviz_cos2(pca, choice="var", axes=1:3)

fviz_contrib(pca, choice="var", axes=1:4)
fviz_cos2(pca, choice="var", axes=1:4)

fviz_contrib(pca, choice="var", axes=1:5)
fviz_cos2(pca, choice="var", axes=1:5)
```


## **Question 10**
Calculate and interpret the contributions for the individuals number 10 and 12 on the factorial axis 1 and 2.
We can extract the contribution and cos2 values’results on the factorial axis 1 and 2 and look at the results only for the individuals 10 and 12. 
As a reminder, the average contribution is 100/27= around 4.

### Code that shows the contributions and cos2 values of the individuals, then the bar charts of these values on the factorial axis 1 and 2 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
resultat_individu$contrib
resultat_individu$cos2
fviz_contrib(pca, choice="ind", axes=1:2)
fviz_cos2(pca, choice="ind", axes=1:2)
```
We can observe that the individual number 10 has a contribution of about 15% on the factorial axis 1 and 2, and that the individual number 12 has a contribution of about 14% on the same factorial axis. In addition, we see that these two individuals has the highest contributions values on these factorial axis compared to the other individuals. If we look at the factorial axis separately like in the previous question, we see that the individual 10 contributes to the axis 1 at 19% and 7% for the second axis while the individual number 12 contributes to the first axis at 21% and 0% to the second axis. 
If we look at the cos2 values on the bar chart, we notice that the cos2 for the individual 10 is the second highest and that the cos2 value of the individual 12 is the fifth highest.
Therefore, those 2 individuals has a contribution higher than the average contribution on the factorial axis 1 and 2. And their cos2 are also high, so those 2 individuals represent those 2 factorial axis correctly.

## **Question 11**
Which of the individuals explain the factorial axis 1,2 and 3 ? 
In the same way as the previous question, we can look at the contributions and cos2 values for all the individuals. Then we can also do graphics in order to vizualise them and their values including for the third dimension here.

### Code that shows the bar charts of the contributions and cos2 values on the factorial axis 1,2 and 3
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
fviz_contrib(pca, choice="ind", axes=1:2)
fviz_contrib(pca, choice="ind", axes=2:3)
fviz_contrib(pca, choice="ind", axes=1:3)

fviz_cos2(pca, choice="ind", axes=1:2)
fviz_cos2(pca, choice="ind", axes=2:3)
fviz_cos2(pca, choice="ind", axes=1:3)
```

If we look at the bar chart for the factorial axis 1-2-3, we see that the individuals 10, 12, 23, 24, 21, 13, 1 and 9 have a contribution higher than the average contribution. Among them, the individuals 21, 13, 10, 23, 9, 12, 1, 22, 20, 19, 5, 7, 24, 18 and 16 have the highest cos2 values. Therefore, the individuals that have the highest cos2 values and a contribution higher than the average contribution are the individuals 10, 12,23, 24, 21, 13, 1 and 9 on the factorial axis 1-2-3. So those individuals represent those factorial axis correctly.




## **Question 12**
Can we sort individuals into groups ? 
We are looking for groups with individuals that have a similar behaviour, which means similar grades in the different topics/subjects.

### Code that shows the variables'standardization and the min and max number of clusters that we can make
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
mydata <- na.omit(file_eleves) # On supprime les NA
mydata <- scale(mydata) # On standardise les variables


# On détermine le nombre de clusters possibles
wss <- (nrow(mydata)-1)*sum(apply(mydata,2,var))

for (i in 2:15) wss[i] <- sum(kmeans(mydata, 
                                     centers=i)$withinss)

plot(1:15, wss, type="b", xlab="Number of Clusters",
     ylab="Within groups sum of squares")

```
Therefore, we know that we can make around 14 clusters max with our file. 
Afterwards, I present a dendogram.

### Code that shows the dendogram for the individuals 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
# Ward Hierarchical Clustering
d <- dist(mydata, method = "euclidean") # distance matrix
fit <- hclust(d, method="ward.D2") 
plot(fit) # display dendogram
```
We see that we can have two big groups (one on the right and one on the left). Or we can have 5 groups for example if we separate the left block into 2 groups, and if we separate the right block into 3 groups. The more we take small height, the more number of groups we have and the other way around, the more height we take, the more small number of groups we have.

### Code that shows the dendogram with 5 groups put in evidence
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
# CLuster avec la méthode de Ward
d <- dist(mydata, method = "euclidean") 
fit <- hclust(d, method="ward.D2") 
plot(fit) 
groups <- cutree(fit, k=5) 
rect.hclust(fit, k=5, border="red")
```
Here, I put in evidence 5 groups with red delimitation for example. But like I said before, it's completely possible to make more or less groups. 

### Code that shows the 5 groups on the factorial axis 1 and 2 
```{r message=FALSE, warning=FALSE, paged.print=FALSE}
# K-Means Clustering with 5 clusters
fit <- kmeans(mydata, 5)
# vary parameters for most readable graph
library(cluster) 
clusplot(mydata, fit$cluster, color=TRUE, shade=TRUE, 
         labels=2, lines=0)
```
Here we can see the 5 groups on the factorial axis 1 and 2. The group 1 contains the individuals 5, 7, 9, 3, 18, 15 (more or less), the second group contains the individuals 20, 6, 17, 1. After, the third group contains the individuals 26, 16, 19, 25, 14, 4 (more or less), the fourth the individuals 10, 21 et 13 and the last group is composed by the individuals 22, 24, 12, 23 (more or less).

## **Question 12 Suite**
Do a recap of the PCA results. 
Can we deduce tendancies concerning the topics/subjects ? 

To conclude, we can say that around 34% of the total variance is explained by the first dimension, and about 51% by the dimensions 1 and 2. 
The first factorial axis is mainly represented by the variables orth, expr, math, angl, hist, edmu, and arts while the second is mainly represented by the variables/topics reci and angl. 

Concerning the individuals, the first factorial axis is mainly represented by the individuals 1, 9, 10, 12, 21, and 24 while the second is mainly represented by the individuals 13, 16, 19 and 22.
In addition, it's possible to make 14 groups max. I chose to make 5 groups. 
The individuals inside a group have similar behaviour concerning their grades, while the individuals in different groups have different behaviour. 








































